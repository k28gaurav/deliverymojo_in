package com.traconmobi.tom;

//import org.eclipse.jdt.annotation.Nullable;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by hp1 on 21-01-2015.
 */
public class RVPOrderFragment<EmployeeAction>  extends Fragment {

    SessionManager session;
    public String SESSION_TRANSPORT, employeeId, session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, session_DB_PATH, session_DB_PWD;
    TinyDB tiny;
    protected SQLiteDatabase db;
    protected Cursor cur, cursor, sync_count, c_amt_val, cursor_outscan_dt, cr_unattempt_del, cr, cursor_srch, c_chk_consgnee, c_chk_custmr, cursor_shippr_list;
    public static String sync_cnt, sync_cnt_shw, cnt, keyword, keyword_val, Page_Name, screen_id, Assignment_Type, fltr_type, Assgn_type, t_u_id, Assgn_flag, cod_amt_val;
    String strFilter = "", index, address1, address2, uname, addr1, addr2, city_detail, celphno, pincode, awb_outscantime, awb_outscan_dt, cod_flag, cod_amt, str1;
    TextView consignee_Name, consignee_address1,consignee_address2,consignee_city, codval, itemDescription, describedValue, noOfPc, waybill,reasonOfreturn,shipperPin, pickTime;
    protected List<TOMTransactAction> actions;
    protected EmployeeActionAdapter adapter;
    public String consignee_Nme, consignee_addr, consgnee;
    ListView lv;
    Button btn_undel, btn_del;
    StringBuffer responseText;
    public String TAG = "RVPOrderFragment";
    public static int total_awbs;
    public String assignId, NAV_FORM_TYPE_VAL;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_rvppickup_details, container, false);
        try {
            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());
            // Session class instance
            session = new SessionManager(getActivity().getApplicationContext());

            // Session class instance
            tiny = new TinyDB(getActivity().getApplicationContext());

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);

            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            SESSION_TRANSPORT = tiny.getString("transport_val");
            Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_TRANSPORT " + SESSION_TRANSPORT);

            employeeId = this.getArguments().getString("orderid");//getIntent().getStringExtra(TOMContactActivity.EXTRA_MESSAGE);
            assignId =  this.getArguments().getString("pickId");
            NAV_FORM_TYPE_VAL = this.getArguments().getString("NAV_FORM_TYPE_VAL");

            Log.e(TAG, "AssignID:" + assignId);

//        Toast.makeText(getActivity(),"hello"+ employeeId, Toast.LENGTH_LONG).show();
            consignee_Name = (TextView) v.findViewById(R.id.employeeName);
            consignee_city = (TextView) v.findViewById(R.id.city);
            consignee_address1 = (TextView) v.findViewById(R.id.address1);
            consignee_address2 = (TextView) v.findViewById(R.id.address2);
            consignee_city = (TextView) v.findViewById(R.id.city);
            itemDescription = (TextView) v.findViewById(R.id.itemdescr);
            reasonOfreturn = (TextView) v.findViewById(R.id.reasonofreturn);

            shipperPin = (TextView) v.findViewById(R.id.shipperpin);
            noOfPc = (TextView) v.findViewById(R.id.no_of_pcs);
            pickTime = (TextView) v.findViewById(R.id.pickupTime);
            describedValue = (TextView) v.findViewById(R.id.describedvalue);

            lv = (ListView) v.findViewById(R.id.list);
            shwsinglelist();
            btn_undel = (Button) v.findViewById(R.id.btnundel);
            btn_undel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), UndeliverMainActivity.class);
//                startActivity(intent);

                    try {
                        String str_amt = null;
                        String result;
                        int j = 0;
                        double total_cod = 0;
                        String cod = null;
                        responseText = new StringBuffer();
//                    ArrayList<Country> countryList = dataAdapter.countryList;
//						    use for loop i=0 to < countryList.size()
                   /* for(int i=0;i<countryList.size();i++)
                    {
                        Country country = countryList.get(i);
                        if(country.isSelected())
                        {*/
                        responseText.append(employeeId + ",");
                           /* j=j+1;
                        }
                    }*/
                        Crashlytics.log(android.util.Log.ERROR, TAG, "shw accepted assigned list " + responseText.toString());
                        session.createAssgnMultiple_num_Session(responseText.toString());
                        if (responseText.toString().isEmpty() || responseText.toString() == null) {
                            Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                        } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                            try {
                                btn_undel.setEnabled(false);
//                            btn_undel.setBackgroundColor(Color.GRAY);
                                if (session_DB_PATH != null && session_DB_PWD != null) {
                                    if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                   /* Toast.makeText(getActivity(),"Please start the trip", Toast.LENGTH_LONG).show();
                                    btn_undel.setEnabled(true);
//                                    btn_undel.setBackgroundResource(R.drawable.button);*/

                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                                        // Setting Dialog Title
                                        alertDialog.setTitle("Alert");

                                        alertDialog.setCancelable(false);
                                        // Setting Dialog Message
                                        alertDialog.setMessage("Please start the trip");

                                        // On pressing Settings button
                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
//								Toast.makeText(AssignedMultipleUndelActivity.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                btn_undel.setEnabled(true);
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                startActivity(goToNextActivity);
                                            }
                                        });

                                        alertDialog.show();
                                    } else {
                                        if (SESSION_TRANSPORT.equals("BIKER")) {
                                            Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleFailedPickup.class);
                                            session.createAssgnMultiple_num_Session(responseText.toString());
                                            total_awbs = j;
                                            str_amt = "0";
                                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                            goToNextActivity.putExtra("assignId", assignId);
                                            goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                            startActivity(goToNextActivity);
                                            getActivity().finish();
                                        } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                            Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleFailedPickup.class);
                                            session.createAssgnMultiple_num_Session(responseText.toString());
                                            total_awbs = j;
                                            str_amt = "0";
                                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                            goToNextActivity.putExtra("assignId", assignId);
                                            goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                            startActivity(goToNextActivity);
                                            getActivity().finish();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Assigned_Delivery_MainActivity customer locate " + e.getMessage());
                            } finally {
                                if (c_chk_consgnee != null && !c_chk_consgnee.isClosed()) {
                                    c_chk_consgnee.close();
                                }
                                if (c_chk_custmr != null && !c_chk_custmr.isClosed()) {
                                    c_chk_custmr.close();
                                }
                                if (db != null && db.isOpen()) {
                                    db.close();
                                }
                            }
                        }

                    } catch (SQLiteException ex) {
                        ex.getStackTrace();
                        //onClickGoToHomePage();
                    } catch (UnsatisfiedLinkError err) {
                        err.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                        //onClickGoToHomePage();
//		    	        	    			finish();
                    }
                }
            });

            btn_del = (Button) v.findViewById(R.id.btndel);
            btn_del.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        try {

                            String str_amt = null;
                            String result;
                            int j = 0;
                            double total_cod = 0;
                            String cod = null;
                            responseText = new StringBuffer();
                            responseText.append(employeeId + ",");

                            Crashlytics.log(android.util.Log.ERROR, TAG, "shw accepted assigned list " + responseText.toString());
                            session.createAssgnMultiple_num_Session(responseText.toString());

                            if (responseText.toString().isEmpty() || responseText.toString() == null) {
                                Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                            } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                                try {
                                    btn_del.setEnabled(false);
//                            btn_del.setBackgroundColor(Color.GRAY);
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                    /*Toast.makeText(getActivity(),"Please start the trip", Toast.LENGTH_LONG).show();
                                    btn_del.setEnabled(true);
//                                    btn_del.setBackgroundResource(R.drawable.button);*/

                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                                            // Setting Dialog Title
                                            alertDialog.setTitle("Alert");

                                            alertDialog.setCancelable(false);
                                            // Setting Dialog Message
                                            alertDialog.setMessage("Please start the trip");

                                            // On pressing Settings button
                                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
//								Toast.makeText(AssignedMultipleUndelActivity.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                    btn_del.setEnabled(true);
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                    startActivity(goToNextActivity);
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.show();
                                        } else {
                                            if (SESSION_TRANSPORT.equals("BIKER")) {
                                                if (session_DB_PATH != null && session_DB_PWD != null) {
//		    			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                   // db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                    String shw_res[] = responseText.toString().split(",");
                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleSuccessPickup.class);
                                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                                    session.createAssgnMultiple_num_Session(responseText.toString());

                                                    total_awbs = j;
                                                    goToNextActivity.putExtra("assignId", assignId);
                                                    session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                                    startActivity(goToNextActivity);
                                                    getActivity().finish();

                                                } else {
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                                                    //onClickGoToHomePage();
                                                }
                                            } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                                if (session_DB_PATH != null && session_DB_PWD != null) {

                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleSuccessPickup.class);
                                                    //goToNextActivity.putExtra("FORM_TYPE4_VAL", "FORM_MULTIPLE_DEL");
                                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_ASSIGN");
                                                    session.createAssgnMultiple_num_Session(responseText.toString());

                                                    total_awbs = j;
                                                    goToNextActivity.putExtra("assignId", assignId);
                                                    session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                                                    startActivity(goToNextActivity);
                                                    getActivity().finish();

                                                } else {
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                                                    //onClickGoToHomePage();
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "Assigned_Delivery_MainActivity customer locate " + e.getMessage());
                                } finally {
                                    if (c_chk_consgnee != null && !c_chk_consgnee.isClosed()) {
                                        c_chk_consgnee.close();
                                    }
                                    if (c_chk_custmr != null && !c_chk_custmr.isClosed()) {
                                        c_chk_custmr.close();
                                    }
                                    if (db != null && db.isOpen()) {
                                        db.close();
                                    }
                                }
                            }
                        } catch (SQLiteException ex) {
                            ex.getStackTrace();
                            //onClickGoToHomePage();
                        } catch (UnsatisfiedLinkError err) {
                            err.getStackTrace();
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                            //onClickGoToHomePage();
//		            	    			finish();
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Assigned_Delivery_MainActivity submitClicked " + e.getMessage());
                        //onClickGoToHomePage();
                    } catch (UnsatisfiedLinkError err) {
                        err.getStackTrace();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity UnsatisfiedLinkError ");
                        //onClickGoToHomePage();
//	        	    			finish();
                    }
                }
            });
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
        }
        return v;
    }

    /*/*/

    /**************************
     * EXTEND THE SIMPLECURSORADAPTER TO CREATE A CUSTOM CLASS WHERE WE CAN OVERRIDE THE GETVIEW TO CHANGE THE ROW COLORS
     **************************//*
    private class MyCursorAdapter extends SimpleCursorAdapter{
        @SuppressWarnings("deprecation")
        public MyCursorAdapter(Context context, int layout, Cursor c,
                               String[] from, int[] to, int flags) {
            super(context, layout, c, from, to);
        }*/


    class EmployeeActionAdapter extends ArrayAdapter<TOMTransactAction> {
        EmployeeActionAdapter() {
            super(getActivity(), R.layout.activity_tomcontacts_action, actions);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TOMTransactAction action = actions.get(position);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.activity_tomcontacts_action, parent, false);
            TextView label = (TextView) view.findViewById(R.id.label);
            label.setText(action.getLabel());
            TextView data = (TextView) view.findViewById(R.id.data);
            data.setText(action.getData());
            return view;
        }
    }

    public void shwsinglelist() {
        try {
            consignee_Name.setText(session.getConsigneeName(assignId));
            consignee_city.setText(session.getShipperCity(assignId));
            shipperPin.setText(session.getShipperPin(assignId));
            itemDescription.setText("Item Description: " + session.getItemDescription(assignId));
            consignee_address1.setText(session.getAddress1(assignId));
            consignee_address2.setText(session.getAddress2(assignId));
            describedValue.setText("Declared Value: " + session.getDescribedValue(assignId).toString());
            noOfPc.setText("No. of pieces: " + session.getNoPcs(assignId));
            reasonOfreturn.setText("Reason of Return: " + session.getReasonOfReturn(assignId));
            pickTime.setText("Assigned Pickup Time: " + session.getPickTime(assignId));

        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMContactActivity SQLiteException shwsinglelist ");
//    		onClickGoToHomePage();
        }
    }
}