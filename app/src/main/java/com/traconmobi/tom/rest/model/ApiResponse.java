package com.traconmobi.tom.rest.model;

import com.google.gson.annotations.SerializedName;
import com.traconmobi.tom.model.Row;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 12/8/15.
 */

public class ApiResponse {

    @SerializedName("total_rows")
    private Integer totalRows;

    @SerializedName("rows")
    private List<Row> rows = new ArrayList<Row>();


    public List<Row> getRows() {
        System.out.println("in API Response");
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }
}