package com.traconmobi.tom;



import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
//import org.eclipse.jdt.annotation.Nullable;

public class DeliveryCompleteStatus extends Fragment {

    protected SQLiteDatabase db;
    TextView version_name;
    Cursor cursor,cursor_outscan_dt,cr;
    public static String awb_outscantime,awb_outscan_dt,flag,Assignment_Type;
    protected ListAdapter adapter;
    protected ListView deliverydetailsList;
    final String user_loc =TOMLoginUserActivity.u_loc;
    final String user_id=TOMLoginUserActivity.usr_id;
    final String curnt_dt=TOMLoginUserActivity.currentdate;
    public String TAG="DeliveryCompleteStatus";
    // Session Manager Class
    SessionManager session;
    public String session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,session_DB_PATH,session_DB_PWD;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        
        View v = inflater.inflate(R.layout.fragment_deliverycompletestatus,container,false);

        try
        {
            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());
            // Session class instance
            session = new SessionManager(getActivity().getApplicationContext());

            /**GETTING SESSION VALUES**/

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
            if(session_DB_PATH != null && session_DB_PWD != null)
            {
//            db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);

                deliverydetailsList = (ListView)v.findViewById (R.id.list);
//            cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode FROM TOM_Assignments WHERE  T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                cursor_outscan_dt=db.rawQuery("SELECT _id ,D_OutScan_Date,T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode FROM TOM_Assignments WHERE T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                while(cursor_outscan_dt.moveToNext())
                {
                    awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
                    awb_outscan_dt=awb_outscantime.substring(0, 10);
                }
//	        Crashlytics.log(android.util.Log.ERROR,TAG,"AWB_OutScanTime"+ awb_outscantime);
//	        Crashlytics.log(android.util.Log.ERROR,TAG,"awb_outscan_dt"+ awb_outscan_dt);

                if(awb_outscan_dt == null)
                {
//                    Toast.makeText(getActivity(), "No Assignments DELIVERED", Toast.LENGTH_SHORT).show();
                }
                else if(awb_outscan_dt != null && awb_outscan_dt != "")
                {
//	        	if(awb_outscan_dt.equals(session_CURRENT_DT))
//	        {
                    Assignment_Type="D";
                    flag="TRUE";
//	        cr= db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' and T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                    cr= db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                    //                                              new String[]{"%" + searchText.getText().toString() + "%"});
                    int cr_count = cr.getCount();
                    if(cr_count!=0)
                    {
                        Assignment_Type="D";
                        flag="TRUE";
                        //*******************************GETTING THE VALUES FROM THE DATABASE********************************/
//	        cursor = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount_Collected FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                        cursor = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,F_Amount_Collected FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                        //                                              new String[]{"%" + searchText.getText().toString() + "%"});
                        final MyCursorAdapter adapter = new  MyCursorAdapter(
                                getActivity(),
                                R.layout.delivered_cardview_layout,
                                cursor,
                                new String[] {"T_Assignment_Number","T_Contact_number","T_Consignee_Name", "T_Address_Line1", "T_Address_Line2","T_City" ,"T_Pincode","F_Amount_Collected"},
                                //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
                                new int[] {R.id.tv_awb,R.drawable.call,R.id.tv_firstName, R.id.tv_lastName, R.id.tv_address,R.id.tv_city,R.id.tv_pincode,R.id.tv_COD_Amount},0);

                        deliverydetailsList.setAdapter(adapter);

//					deliverydetailsList.setAdapter(adapter);
                        deliverydetailsList.setCacheColorHint(Color.WHITE);
                    }
//	        else
//               {
//            	   Crashlytics.log(android.util.Log.ERROR,TAG,"out of ifloopNo waybills to Display");
//            	   Toast.makeText(Showdeliverydetailsactivity.this,"No Assignments DELIVERED", Toast.LENGTH_SHORT).show();
//               }
//		}
                    else
                    {
                        Crashlytics.log(android.util.Log.ERROR,TAG, "out of ifloopNo waybills to Display");
//                        Toast.makeText( getActivity(),"No Assignments DELIVERED", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Crashlytics.log(android.util.Log.ERROR,TAG, "out of 2ifloop");
//                    Toast.makeText( getActivity(),"No Assignments DELIVERED", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Crashlytics.log(android.util.Log.ERROR,TAG, "Error Showdeliverydetailsactivity TOMLoginUserActivity.file is null ");
                //onClickGoToHomePage();
            }
        }
        catch(SQLiteException e){

            Toast.makeText(getActivity(),"Please try Login again !!! " , Toast.LENGTH_LONG).show();
//            onClickLogOut();

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
//            onClickLogOut();
            Toast.makeText(getActivity(),"Please try Login again !!! " , Toast.LENGTH_LONG).show();

        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR,TAG, "Error Showdeliverydetailsactivity UnsatisfiedLinkError ");
//            onClickLogOut();
        }
        return v;
    }

    /**extend the SimpleCursorAdapter to create a custom class where we
     can override the getView to change the row colors**/
    private class MyCursorAdapter extends SimpleCursorAdapter {

        @SuppressWarnings("deprecation")
        public MyCursorAdapter(Context context, int layout, Cursor c,
                               String[] from, int[] to, int flags) {
            super(context, layout, c, from, to);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            /**get reference to the row */
            View view = super.getView(position, convertView, parent);
            try
            {
                if (view == null)
                {
                    LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view = vi.inflate(R.layout.delivered_cardview_layout, null);

                }

                /**check for odd or even to set alternate colors to the row background*/
                if(position % 2 == 0){
                    view.setBackgroundColor(Color.rgb(255, 255, 255));
                }
                else {
                    view.setBackgroundColor(Color.rgb(255, 255, 255));
                }
            }
            catch(Exception e)
            {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR,TAG, "Exception Showdeliverydetailsactivity MyCursorAdapter " + e.getMessage());
            }
            catch(UnsatisfiedLinkError err)
            {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR,TAG, "Error Showdeliverydetailsactivity UnsatisfiedLinkError ");
//                onClickLogOut();
            }
            return view;
        }

    }
   /* public void toggleMenu(View v)
    {
        Intent homeActivity = new Intent(getActivity(), HomeMainActivity.class);
        startActivity(homeActivity);
    }*/
}
/*

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DeliveryCompleteStatus extends Fragment {
	
	public DeliveryCompleteStatus(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_deliverycompletestatus, container, false);
         
        return rootView;
    }
*/
//}
