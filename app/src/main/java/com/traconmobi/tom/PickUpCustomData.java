package com.traconmobi.tom;

/**
 * Created by kumargaurav on 11/4/15.
 */
public class PickUpCustomData {

    private String mAssignNo = "";
    private String mShipperName = "";
    private String mShipperAccNo = "";
    private String mPickUpTime = "";
    private String mNoOfPcs = "";
    private String mAssignId;
    private String mCompanyId;
    private String mShipAdd1;
    private String mShipAdd2;
    private String mShipPin;
    private String mShipCity;
    private String mShipContact;
    public PickUpCustomData(String assignNo, String shipperName, String shipperAccNo, String pickUpTime, String noOfPcs, String assignId, String companyId) {
        mAssignNo = assignNo;
        mShipperName = shipperName;
        mShipperAccNo = shipperAccNo;
        mPickUpTime = pickUpTime;
        mNoOfPcs = noOfPcs;
        mAssignId = assignId;
        mCompanyId = companyId;
    }

    public PickUpCustomData(String shipperAdd1, String shipperAdd2, String shipPin, String shipCity,String shipContact) {
        mShipAdd1 = shipperAdd1;
        mShipAdd2 = shipperAdd2;
        mShipCity = shipCity;
        mShipPin = shipPin;
        mShipContact = shipContact;
    }
    public String getmNoOfPcs() {
        return mNoOfPcs;
    }

    public void setmNoOfPcs(String mNoOfPcs) {
        this.mNoOfPcs = mNoOfPcs;
    }

    public String getmPickUpTime() {

        return mPickUpTime;
    }

    public void setmPickUpTime(String mPickUpTime) {
        this.mPickUpTime = mPickUpTime;
    }

    public String getmShipperAccNo() {

        return mShipperAccNo;
    }

    public void setmShipperAccNo(String mShipperAccNo) {
        this.mShipperAccNo = mShipperAccNo;
    }

    public String getmShipperName() {

        return mShipperName;
    }

    public void setmShipperName(String mShipperName) {
        this.mShipperName = mShipperName;
    }

    public String getmAssignNo() {

        return mAssignNo;
    }

    public void setmAssignNo(String mAssignNo) {
        this.mAssignNo = mAssignNo;
    }

    public String getmCompanyId() {
        return mCompanyId;
    }

    public void setmCompanyId(String mCompanyId) {
        this.mCompanyId = mCompanyId;
    }

    public String getmAssignId() {

        return mAssignId;
    }

    public void setmAssignId(String mAssignId) {
        this.mAssignId = mAssignId;
    }

    public String getmShipCity() {
        return mShipCity;
    }

    public void setmShipCity(String mShipCity) {
        this.mShipCity = mShipCity;
    }

    public String getmShipContact() {
        return mShipContact;
    }

    public void setmShipContact(String mShipContact) {
        this.mShipContact = mShipContact;
    }

    public String getmShipPin() {

        return mShipPin;

    }

    public void setmShipPin(String mShipPin) {
        this.mShipPin = mShipPin;
    }

    public String getmShipAdd2() {

        return mShipAdd2;
    }

    public void setmShipAdd2(String mShipAdd2) {
        this.mShipAdd2 = mShipAdd2;
    }

    public String getmShipAdd1() {

        return mShipAdd1;
    }

    public void setmShipAdd1(String mShipAdd1) {
        this.mShipAdd1 = mShipAdd1;
    }
}
