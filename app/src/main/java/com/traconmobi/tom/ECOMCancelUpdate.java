package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kumargaurav on 2/8/16.
 */
public class ECOMCancelUpdate extends Activity {
    private Button update, back;
    private EditText remarkText;
    private ArrayList<String> get_reasonOPtion;
    private ImageView clearSearch;
    private String[] get_reason;
    private String[] get_reasonId;
    SessionManager sManager = null;
    SharedPreferences pref;
    String pickUpId = "";
    String assignId = "";
    String companyId = "";
    CouchBaseDBHelper dbHelper;
    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "ScanPref";
    public static final String PREF_NAME1 = "Pager";
    public String FORM_TYPE_VAL;
    String reason=" ";
    TextView title;
    public static String TAG = "ECOMCancelUpdate";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        try
        {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_pickup_cancel);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_pickup_cancel);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("PICKUP CANCEL LIST");

            final AutoCompleteTextView autoSearch = (AutoCompleteTextView) findViewById(R.id.reason);
            remarkText = (EditText) findViewById(R.id.remark);
            update = (Button) findViewById(R.id.update);
//        back = (Button)findViewById(R.id.back);

            clearSearch = (ImageView) findViewById(R.id.clear);
            dbHelper = new CouchBaseDBHelper(getApplicationContext());

            pref = getApplicationContext().getSharedPreferences(ParsePickupOutscanXML.PREF_NAME, Context.MODE_PRIVATE);
            Set<String> reasonList = pref.getStringSet("reasonlist", null);
            Set<String> reasonId = pref.getStringSet("reasonId", null);

      /*  Set<String> setPickUp = pref.getStringSet("pickuplist", null);
        Crashlytics.log(Log.ERROR, TAG, "PickUpList" + setPickUp);

        //dataId = new ArrayList<>();

        Object[] dataId = setPickUp.toArray(new String[setPickUp.size()]);*/
            pickUpId = getIntent().getStringExtra("assignmentNo");
            assignId = getIntent().getStringExtra("assignmentId");
            companyId = getIntent().getStringExtra("companyId");

            //dataId = new ArrayList<>();
            get_reason = reasonList.toArray(new String[reasonList.size()]);
            get_reasonId = reasonId.toArray(new String[reasonId.size()]);
            //get_reason = new String[]{"", "", "", ""};
            //get_reasonOPtion = new ArrayList<String>(Arrays.asList(get_reason));
            sManager = new SessionManager(getApplicationContext());
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, get_reason);
            autoSearch.setAdapter(adapter);
            autoSearch.setThreshold(1);
            final String[] reasonText = {""};
            final String[] reasonid = {""};
            autoSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                    reasonText[0] = (String) parent.getItemAtPosition(position);
                    reasonid[0] = get_reasonId[position];
                }
            });

            autoSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    reasonText[0] = " ";
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            clearSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoSearch.setText("");
                }
            });
            update.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              final String remark = remarkText.getText().toString();

                                              final String assign_Id = assignId;
                                              final String assignNo = pickUpId;
                                              final String companyID = companyId;

                                              Date curDate = new Date();
                                              SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                                              final String mLastUpdateTime = dateFormat.format(curDate).toString();

                                              final String docId = assignNo+"_"+assign_Id+"_"+companyID +"_"+ mLastUpdateTime;
                                              final List<String> scanItem = new ArrayList<>();
                                         /* if(autoSearch.getText().toString().trim().isEmpty())
                                          {
                                              Toast.makeText(ECOMCancelUpdate.this,"Please Select Reason",Toast.LENGTH_SHORT).show();
                                          }
                                          else
                                          {*/
                                              reason = reasonText[0];

                                              AlertDialog.Builder alertDialog = new AlertDialog.Builder(ECOMCancelUpdate.this);

                                              // Setting Dialog Title
                                              alertDialog.setTitle("Alert");

                                              // Setting Dialog Message
                                              alertDialog.setMessage("Are you sure you want to update ?");

                                              // On pressing Settings button
                                              alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                  @Override
                                                  public void onClick(DialogInterface dialog, int which) {
                                                      try {
                                                          if (reason.isEmpty()) {
                                                              Toast.makeText(getApplicationContext(), "Please select reason", Toast.LENGTH_LONG).show();
                                                          } else {
                                                              List<String> reasonList = Arrays.asList(get_reason);
                                                              if (reasonList.contains(reason)) {
                                                                  sManager.setReason(assignNo, reason);
                                                                  dbHelper.updateECOMDetails(docId, "N", scanItem, mLastUpdateTime, reason, remark, companyID, sManager.getLocId(), sManager.getuserId(), assignNo, sManager.getDocType(assignNo),reasonid[0]);

                                                                  //ECOMCancelUpdate.this.finish();
                                                              } else {
                                                                  Toast.makeText(getApplicationContext(), "Select reason from the list", Toast.LENGTH_LONG).show();
                                                              }
                                                          }
                                                      } catch (Exception e) {
                                                          Crashlytics.log(Log.ERROR, TAG, "Error Exception Onend_trip " + e.getMessage());
                                                      }

                                                     /* if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_ASSIGN")) {
                                                          //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                                          pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                                          editor = pref.edit();
//                        editor.clear();
                                                          editor.putString("position", String.valueOf(1));
                                                          // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                                          editor.commit();
                                                          Intent homeActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                                                          startActivity(homeActivity);
                                                      } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_COMPLETE")) {
                                                          //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                                          pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                                          editor = pref.edit();
//                        editor.clear();
                                                          editor.putString("position", String.valueOf(1));
                                                          // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                                          editor.commit();
                                                          Intent homeActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                                                          startActivity(homeActivity);
                                                      } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_INCOMPLETE")) {
                                                          //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
                                                          pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
                                                          editor = pref.edit();
//                        editor.clear();
                                                          editor.putString("position", String.valueOf(1));
                                                          // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                                          editor.commit();
                                                          Intent homeActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                                                          startActivity(homeActivity);
                                                      }*/
                                                      dialog.dismiss();
                                                      finish();
                                                      return;
//	            	dialog.cancel();
                                                      //            	onClickLogOut();

//	        		else
//	        		{
//	        		Intent intent = new Intent(Customer_locateMainActivity.this, Conveyance_CaptureMainActivity.class);
//	        		intent.putExtra(CUST_LOCATE_ASSGN_ID, locte_assgn_id);
//	        		startActivity(intent);
//	        		}
                                                  }
                                              });

                                              // on pressing cancel button
                                              alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                  @Override
                                                  public void onClick(DialogInterface dialog, int which) {
                                                      dialog.dismiss();
                                                  }
                                              });

                                              // Showing Alert Message
                                              alertDialog.create().show();


                       /* AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getApplicationContext());

                        // set title
                        alertDialogBuilder.setTitle("Alert");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Press Ok to update")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, close
                                        // current activity
                                        dbHelper.updateDoc(reason, get_reason, assignNo, assign_Id, companyID);
                                        ECOMCancelUpdate.this.finish();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialogBuilder.show();*/
//                                      }
                                          }
                                      }

            );
        }
        catch(Exception e)

        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());

        }
    }
    public void toggleMenu(View v)
    {
        finish();
       /* pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.clear();
        editor.putString("position", String.valueOf(1));
        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
        editor.commit();
        Intent homeActivity = new Intent(ECOMCancelUpdate.this, AssignedListMainActivity.class);
        startActivity(homeActivity);*/
    }

    @Override
    public void onBackPressed()
    {

    }
}
