package com.traconmobi.tom;

//import android.app.Fragment;
import android.app.ProgressDialog;
import android.database.SQLException;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.adapter.DeliveryAdapter;
import com.traconmobi.tom.adapter.IncompleteDeliverAdapter;
import com.traconmobi.tom.adapter.ItemRow;


//import net.sourceforge.zbar.android.CameraTest.CameraTestActivity;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.widget.AdapterView.OnItemClickListener;

import io.fabric.sdk.android.Fabric;

public class DeliveryIncompleteStatus extends Fragment {

    // Session Manager Class
    SessionManager session;
    List<ItemRow> items;
    IncompleteDeliverAdapter deliveryAdapter;
    public String session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,session_DB_PATH,session_DB_PWD;
    TinyDB tiny;
    protected SQLiteDatabase db;
    protected Cursor cur,cursor,sync_count,c_amt_val,cursor_outscan_dt,cr_unattempt_del,cr,cursor_srch;
    public static String sync_cnt,sync_cnt_shw,cnt,keyword,keyword_val,Page_Name,screen_id,Assignment_Type,fltr_type,Assgn_type,t_u_id,Assgn_flag,cod_amt_val;
    String strFilter="",index,address1,address2,uname,addr1,addr2,city_detail,celphno,pincode,awb_outscantime,awb_outscan_dt,cod_flag,cod_amt,str1;

    protected ListView employeeList;
    protected EditText searchText;

    static final String EXTRA_MESSAGE = null;

    static String indx;
    public DeliveryIncompleteStatus(){}
    public static int total_awbs;
    public String TAG="DeliveryIncompleteStatus";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) 
    {

        View rootView = inflater.inflate(R.layout.fragment_deliveryincompletestatus, container, false);
        try
        {
        //Intializing Fabric
        Fabric.with(getActivity(), new Crashlytics());
        // Session class instance
        session = new SessionManager(getActivity().getApplicationContext());

        // Session class instance
        tiny =new TinyDB(getActivity().getApplicationContext());

        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


        // get user data from session
        HashMap<String, String> login_Dts = session.getLoginDetails();

        // Userid
        session_user_id = login_Dts.get(SessionManager.KEY_UID);

        // pwd
        session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // session_USER_LOC
        session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);

        // session_USER_NUMERIC_ID
        session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

        // session_CURRENT_DT
        session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);

        // session_CUST_ACC_CODE
        session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);

        // session_USER_NAME
        session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);


        searchText = (EditText)rootView.findViewById(R.id.searchText);
        employeeList = (ListView)rootView.findViewById(R.id.list);
            items = new ArrayList<ItemRow>();
            deliveryAdapter = new IncompleteDeliverAdapter(getActivity(), getActivity().getApplicationContext(), items);
            employeeList.setAdapter(deliveryAdapter);
            employeeList.setCacheColorHint(Color.WHITE);
            new LoadTask().execute();


            //shwsinglelist();
    }
    catch(Exception e)
    {
        Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
    }
        return rootView;
    }

    //@Override
    public void onBackPressed() {

    }
    public void shwsinglelist()
    {
        try
        {
            if(session_DB_PATH != null && session_DB_PWD != null)
            {
                db= SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                Assignment_Type="D";
                fltr_type="A";
                String flag="FALSE";
                cursor_outscan_dt=db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,F_Amount,D_OutScan_Date,T_Pincode,F_Amount_Collected FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                while(cursor_outscan_dt.moveToNext()){
                    awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
                    awb_outscan_dt=awb_outscantime.substring(0, 10);
                    cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
                    session.createSingleCODSession(cod_amt_val);
                }
                cursor_outscan_dt.close();
                db.close();
                if(awb_outscan_dt == null)
                {
                }else if(awb_outscan_dt != null)
                {
                    db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
                    Assignment_Type="D";
                    fltr_type="A";
                    flag="FALSE";
                    cur = db.rawQuery("SELECT del._id , del.T_Assignment_Number,del.T_Assignment_Type,del.T_Consignee_Name,del.T_Address_Line1, del.T_Address_Line2 ,del.T_City ,del.T_Contact_number,del.T_Pincode,del.F_Amount,del.T_Amount_Type,del.F_Amount_Collected,undel.T_Assignment_In_Complete_Reason  FROM TOM_Assignments del JOIN Tom_Assignments_In_Complete  undel ON undel.T_Assignment_Id = del.T_Assignment_Id WHERE del.T_Assignment_Type='"+ Assignment_Type  +"' AND del.B_is_Completed='"+ flag +"' AND del.T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND del.D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                    //                                              new String[]{"%" + searchText.getText().toString() + "%"});
 /*                   final MyCursorAdapter adapter  = new  MyCursorAdapter(
                            getActivity(),
//				                        R.layout.row,
                            R.layout.undelivery_list_items,
                            cur,
                            new String[] {"T_Assignment_Number","T_Contact_number","T_Consignee_Name","T_Address_Line1","F_Amount","T_Amount_Type","T_Assignment_In_Complete_Reason"},
                            //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
                            new int[] {R.id.txtordrval,R.drawable.call,R.id.txtName,R.id.txtADDR,R.id.txtrsval,R.id.cod,R.id.txtreasonval},0);

                    /*//***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************//*
                    employeeList.setAdapter(adapter);
                    employeeList.setCacheColorHint(Color.WHITE);
                    employeeList.setOnItemClickListener(new OnItemClickListener(){
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,long id) {

//                            Toast.makeText(getActivity(),"hello", Toast.LENGTH_LONG).show();
                            // TODO Auto-generated method stub
                            Intent intent = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                            Cursor cursor = (Cursor) adapter.getItem(position);
                            intent.putExtra("EMPLOYEE_ID", cursor.getInt(cursor.getColumnIndex("_id")));
                            index = cur.getString(cur.getColumnIndex("T_Assignment_Number"));
                            String cod=null;
                            String str_amt = null ;
                            double total_cod =0;
                            cod=cur.getString(cur.getColumnIndex("F_Amount"));
                            intent.putExtra("FORM_TYPE4_VAL","FORM_MULTIPLE_UNDEL");
                            session.createAssgnMultiple_num_Session(index);
                            total_cod = total_cod + Double.valueOf(cod);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(total_cod);
                            total_awbs = cur.getCount();
                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
//                            session.createAssgnMultiple_num_Session(responseText.toString());
                            indx=cur.getString(cur.getColumnIndex("_id"));
                            intent.putExtra(EXTRA_MESSAGE, index);
                            startActivity(intent);
                        }
                    });*/
                    db.close();
                }
                else{
//                    Toast.makeText(getActivity(),"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
                }
//        db.close();
            }
            else
            {
                Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity step3 shwsinglelist TOMLoginUserActivity.file is null ");
//    			onClickGoToHomePage();
            }
        }
        catch(SQLiteException e){
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity SQLiteException shwsinglelist ");
//    		onClickGoToHomePage();
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity UnsatisfiedLinkError shwsinglelist ");
//    		onClickGoToHomePage();
        }
        finally {

            if (db != null)
            {
                db.close();
            }
        }

    }

    private class LoadTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog((AssignmentInCompleteStatus)getActivity());

        LoadTask() {
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Deliver list items to prepare...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }

        @Override
        protected Void doInBackground(Void... param) {
            try {
                if(session_DB_PATH != null && session_DB_PWD != null)
                {
                    db= SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    Assignment_Type="D";
                    fltr_type="A";
                    String flag="FALSE";
                    cursor_outscan_dt=db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,F_Amount,D_OutScan_Date,T_Pincode,F_Amount_Collected FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                    while(cursor_outscan_dt.moveToNext()){
                        awb_outscantime=cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("D_OutScan_Date"));
                        awb_outscan_dt=awb_outscantime.substring(0, 10);
                        cod_amt_val = cursor_outscan_dt.getString(cursor_outscan_dt.getColumnIndex("F_Amount"));
                        session.createSingleCODSession(cod_amt_val);
                    }
                    cursor_outscan_dt.close();
                    db.close();
                    if(awb_outscan_dt == null)
                    {
                    }else if(awb_outscan_dt != null)
                    {
                        db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
                        Assignment_Type="D";
                        fltr_type="A";
                        flag="FALSE";
                        cur = db.rawQuery("SELECT del._id , del.T_Assignment_Number,del.T_Assignment_Type,del.T_Consignee_Name," +
                                "del.T_Address_Line1, del.T_Address_Line2 ,del.T_City ,del.T_Contact_number,del.T_Pincode,del.F_Amount," +
                                "del.T_Amount_Type,del.F_Amount_Collected,undel.T_Assignment_In_Complete_Reason  " +
                                "FROM TOM_Assignments del JOIN Tom_Assignments_In_Complete  undel ON undel.T_Assignment_Id " +
                                "= del.T_Assignment_Id WHERE del.T_Assignment_Type='"+ Assignment_Type  +"' AND del.B_is_Completed='"+
                                flag +"' AND del.T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND del.D_OutScan_Date='" +
                                session_CURRENT_DT +"'", null);


                        while (cur.moveToNext()) {
                            items.add(new ItemRow(cur.getString(cur.getColumnIndex("_id")), cur.getString(cur.getColumnIndex("T_Assignment_Number")),
                                    cur.getString(cur.getColumnIndex("T_Contact_number")), cur.getString(cur.getColumnIndex("T_Consignee_Name")),
                                    cur.getString(cur.getColumnIndex("T_Address_Line1")), cur.getString(cur.getColumnIndex("T_Address_Line2")),
                                    cur.getString(cur.getColumnIndex("F_Amount")), cur.getString(cur.getColumnIndex("T_City")),
                                    cur.getString(cur.getColumnIndex("T_Amount_Type")),cur.getString(cur.getColumnIndex("T_Assignment_In_Complete_Reason"))));
                        }

                        //                                              new String[]{"%" + searchText.getText().toString() + "%"});
 /*                   final MyCursorAdapter adapter  = new  MyCursorAdapter(
                            getActivity(),
//				                        R.layout.row,
                            R.layout.undelivery_list_items,
                            cur,
                            new String[] {"T_Assignment_Number","T_Contact_number","T_Consignee_Name","T_Address_Line1","F_Amount","T_Amount_Type","T_Assignment_In_Complete_Reason"},
                            //  new String[] {"OS_AWB_No","Consignee_Contact_Number","Consignee_Name", "Address_Line_1", "Address_Line_2","City" ,"Pincode"},
                            new int[] {R.id.txtordrval,R.drawable.call,R.id.txtName,R.id.txtADDR,R.id.txtrsval,R.id.cod,R.id.txtreasonval},0);

                    /*//***********************VIEW BINDER IS USED TO DISPLAY THE AMOUNT IN LISTVIEW***********************//*
                    employeeList.setAdapter(adapter);
                    employeeList.setCacheColorHint(Color.WHITE);
                    employeeList.setOnItemClickListener(new OnItemClickListener(){
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,long id) {

//                            Toast.makeText(getActivity(),"hello", Toast.LENGTH_LONG).show();
                            // TODO Auto-generated method stub
                            Intent intent = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                            Cursor cursor = (Cursor) adapter.getItem(position);
                            intent.putExtra("EMPLOYEE_ID", cursor.getInt(cursor.getColumnIndex("_id")));
                            index = cur.getString(cur.getColumnIndex("T_Assignment_Number"));
                            String cod=null;
                            String str_amt = null ;
                            double total_cod =0;
                            cod=cur.getString(cur.getColumnIndex("F_Amount"));
                            intent.putExtra("FORM_TYPE4_VAL","FORM_MULTIPLE_UNDEL");
                            session.createAssgnMultiple_num_Session(index);
                            total_cod = total_cod + Double.valueOf(cod);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(total_cod);
                            total_awbs = cur.getCount();
                            session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
//                            session.createAssgnMultiple_num_Session(responseText.toString());
                            indx=cur.getString(cur.getColumnIndex("_id"));
                            intent.putExtra(EXTRA_MESSAGE, index);
                            startActivity(intent);
                        }
                    });*/
                        cur.close();
                        db.close();
                    }
                    else{
//                    Toast.makeText(getActivity(),"No outscan Deliveries for today,\n please contact Authorized Executive", Toast.LENGTH_LONG).show();
                    }
//        db.close();
                }
            }catch(SQLException e) {
                if(db != null) {
                    db.close();
                }
            }
            catch (Exception e) {
                Log.e("DeliverySingleFragment", "" + e.getMessage());
                if(db != null) {
                    db.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            deliveryAdapter.notifyDataSetChanged();
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        }
    }

    //**************************EXTEND THE SIMPLECURSORADAPTER TO CREATE A CUSTOM CLASS WHERE WE CAN OVERRIDE THE GETVIEW TO CHANGE THE ROW COLORS**************************/
    private class MyCursorAdapter extends SimpleCursorAdapter{
        @SuppressWarnings("deprecation")
        public MyCursorAdapter(Context context, int layout, Cursor c,
                               String[] from, int[] to, int flags) {
            super(context, layout, c, from, to);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            //***********************get reference to the row **********************/
            View view = super.getView(position, convertView, parent);
            if (view == null){
                LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = vi.inflate(R.layout.undelivery_list_items, null);
            }
            try
            {
//                indx = cur.getString(cur.getColumnIndex("_id"));

                indx = cur.getString(cur.getColumnIndex("_id"));
                ImageView textdir = (ImageView)view.findViewById(R.id.btnmap);

                /************************ CALLED WHEN USER CLICKS GET DIRECTION TEXTLINK ************************/
                textdir.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v){
                        indx = cur.getString(cur.getColumnIndex("_id"));
                        @SuppressWarnings("unused")
                        int selectedid  =  position;
                        RelativeLayout rl=(RelativeLayout)v.getParent();
                        TextView textawb=(TextView)rl.findViewById(R.id.txtordrval);
                        String getawb =textawb.getText().toString();

//                    db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
                        Cursor csr = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Consignee_Name,emp.T_Address_Line1, emp.T_Address_Line2,emp.T_City,emp.T_Contact_number,emp.T_Pincode FROM TOM_Assignments emp where emp.T_Assignment_Number = ?",
                                new String[]{""+getawb});

                        while (csr.moveToNext()){
                            address1 =csr.getString(csr.getColumnIndex("T_Address_Line1"));
                            address2 =csr.getString(csr.getColumnIndex("T_Address_Line2"));
                            city_detail =csr.getString(csr.getColumnIndex("T_City"));
                        }
                        csr.close();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?daddr=" + address1 + address2 + city_detail + "&dirflg=r"));
//                        if (isAppInstalled("com.google.android.apps.maps")){
//                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                        }
                        db.close();
                        startActivity(intent);
//                    finish()
                    }
                });
//            db.close();
                /********************** CALLED WHEN USER CLICKS PHONENUMBER TEXTLINK *********************/

                ImageView txtcellphn =(ImageView)view.findViewById(R.id.btncall);
                txtcellphn.setOnClickListener(new OnClickListener(){
                    @Override
                    @SuppressWarnings("static-access")
                    public void onClick(View v) {
                        Crashlytics.log(android.util.Log.ERROR,TAG,"entry to call");
                        RelativeLayout rl=(RelativeLayout)v.getParent();
                        TextView textawb=(TextView)rl.findViewById(R.id.txtordrval);
                        String getawb =textawb.getText().toString();
                        Crashlytics.log(android.util.Log.ERROR,TAG,"getawb"+getawb);

//                    db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
                        Cursor cr = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Consignee_Name,emp.T_Address_Line1,emp.T_Address_Line2,emp.T_City,emp.T_Contact_number,emp.T_Pincode FROM TOM_Assignments emp where emp.T_Assignment_Number = ?",
                                new String[]{""+getawb});
                        while (cr.moveToNext()){
                            celphno = cr.getString(cr.getColumnIndex("T_Contact_number"));
                        }
                        cr.close();
                        PhoneCall phcl = new PhoneCall();
                        phcl.call(celphno, getActivity());
//                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                    callIntent.setData(Uri.parse("tel:" + celphno));
//                    startActivity(callIntent);
                        db.close();


                    }
                });

                //***********************CHECK FOR ODD OR EVEN TO SET ALTERNATE COLORS TO THE ROW BACKGROUND**********************/
                if(position % 2 == 0){
                    view.setBackgroundColor(Color.rgb(255, 255, 255));
                    //  view.setBackgroundColor(color.opaque_back);
                }else {
                    view.setBackgroundColor(Color.rgb(255, 255, 255));
                }
            }
            catch(Exception e)
            {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR,TAG,"Exception getView " + e.getMessage());
            }
            catch(UnsatisfiedLinkError err)
            {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMContactActivity UnsatisfiedLinkError ");
//        		onClickGoToHomePage();
            }
            return view;
        }
    }
}





